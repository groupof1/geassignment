package Alex.GeAssignment;

import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static Alex.GeAssignment.Utils.retrieveResourceFromResponse;

public class PublicApiTest {
    private CloseableHttpClient client;
    private HttpResponse response;

    @BeforeClass
    public void setUp() {
        client = HttpClientBuilder.create().build();
    }


    @Test
    @Parameters("url")
    public void getResponseTest(String url) throws IOException {
        // Given
        HttpUriRequest request = new HttpGet(url);

        // When
        response = client.execute(request);

        //Then
        Assert.assertEquals(response.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "getResponseTest")
    @Parameters({"property", "value"})
    public void searchForPropertyTest(String property, String value) throws IOException {
        // Given
        //response. (Previous test succeeded (getResponseTest))

        // When
        RequestWrapper requestWrapper = retrieveResourceFromResponse(response, RequestWrapper.class);
        List<Map<String, String>> matchingEntries = new ArrayList<>();
        for (Map<String, String> entry : requestWrapper.getEntries()) {
            if (entry.containsKey(property) && entry.get(property).equals(value)) {
                matchingEntries.add(entry);
            }
        }

        // Then
        Assert.assertFalse(matchingEntries.isEmpty());
        matchingEntries.forEach(System.out::println);
    }

    @AfterClass(alwaysRun = true)
    private void destroy() throws IOException {
        client.close();
    }


}

@Data
class RequestWrapper {
    private List<Map<String, String>> entries;
}
