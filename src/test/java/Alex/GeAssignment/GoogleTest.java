package Alex.GeAssignment;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.TestException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;


public class GoogleTest {
    private WebDriver driver;
    private List<String> links;

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions().setHeadless(true);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void getGoogleTest() {
        //Given
        //Setup successful

        //When
        driver.get("https://www.google.co.il/");

        //Then
        //Nothing breaks
    }

    @Test(dependsOnMethods = {"getGoogleTest"})
    @Parameters("searchTerm")
    public void searchInGoogleTest(String searchTerm) {
        //Given
        searchTerm = searchTerm.trim();

        //When
        WebElement queryBox = driver.findElement(By.name("q"));
        queryBox.sendKeys(searchTerm); //I would prefer to append \n to searchTerm instead of next few lines, but the task explicitly instructs to click the button :)

        //close suggestions, so they don't move search button and cause wrong search term or "click intercepted exception"
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(500));
        wait.until(driver -> driver.findElement(By.className("UUbT9")).isDisplayed());
        queryBox.sendKeys(Keys.ESCAPE);
        driver.findElements(By.name("btnK")).stream().filter(WebElement::isDisplayed).findFirst().orElseThrow(() -> new TestException("Did not find search button!")).click();

        //Then
        //Nothing breaks
    }

    @Test(description = "Prints all main links' text.", dependsOnMethods = {"searchInGoogleTest"})
    public void getLinksTest() {
        //Given
        //Previous test succeeded (searchInGoogle)

        //When
        links = driver.findElements(By.xpath("//div[@id='search']//a/h3"))
                .stream()
                .map(WebElement::getText)
                .filter(t -> !t.isEmpty()).collect(Collectors.toList());

        //Then
        Assert.assertFalse(links.isEmpty(), "Didn't find any results!");
        for (String text : links) {
            System.out.println(text);
        }
    }

    @Test(description = "Checks whether first link contains a specific word.", dependsOnMethods = {"getLinksTest"})
    @Parameters("firstResultValue")
    public void firstResultIsTest(String firstResultValue) {
        System.out.printf("Checking that first search result contains '%s'\n", firstResultValue);

        //Given
        //links. (Previous test succeeded (getLinksTest))

        //When
        String firstLinkText = links.get(0);

        //Then
        Assert.assertTrue(firstLinkText.toLowerCase().contains(firstResultValue.toLowerCase()), String.format("First result does not contain word %s!", firstResultValue));
    }

    @Test(description = "Checks whether last link does not contains a specific word.", dependsOnMethods = {"getLinksTest"})
    @Parameters("lastResultValue")
    public void lastResultIsNotTest(String lastResultValue) {
        System.out.printf("Checking that last search result does not contain '%s'\n", lastResultValue);

        //Given
        //links. (Previous test succeeded (getLinksTest))

        //When
        String lastLinkText = links.get(links.size() - 1);

        //Then
        Assert.assertFalse(lastLinkText.toLowerCase().contains(lastResultValue.toLowerCase()), String.format("First result does not contain word %s!", lastResultValue));
    }


    @AfterClass(alwaysRun = true)
    public void destroy() {
        driver.quit();
    }
}
